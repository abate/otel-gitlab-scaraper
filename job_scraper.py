#!/usr/bin/env python3

import os
import argparse
import gitlab
import time
from datetime import datetime
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor

def initialize_tracer(otel_collector_endpoint):
    trace.set_tracer_provider(TracerProvider())

    # Initialize OTLP exporter for OpenTelemetry Collector
    otlp_exporter = OTLPSpanExporter(endpoint=otel_collector_endpoint)

    # Create a BatchSpanProcessor and add the exporter to it
    span_processor = BatchSpanProcessor(otlp_exporter)

    # Add the processor to the trace provider
    trace.get_tracer_provider().add_span_processor(span_processor)

def iso8601_to_seconds(timestamp_str):
    try:
        timestamp_dt = datetime.strptime(timestamp_str, '%Y-%m-%dT%H:%M:%S.%fZ')
        timestamp_sec = int(timestamp_dt.timestamp())
        return timestamp_sec
    except ValueError as e:
        print(f"Error parsing timestamp: {e}")
        return None

def fetch_pipeline_jobs_info(gl, project_id, pipeline_id):

    try:
        project = gl.projects.get(project_id)
        pipeline = project.pipelines.get(pipeline_id)

        jobs_info = []

        # Iterate through jobs and extract information
        for job in pipeline.jobs.list():
            print(job)
            if job.started_at:
                job_info = {
                    "job_id": job.id,
                    "name": job.name,
                    "status": job.status,
                    "started_at": iso8601_to_seconds(job.started_at),
                    "finished_at": iso8601_to_seconds(job.finished_at)
                    # Add more job information as needed
                }
                jobs_info.append(job_info)

        return pipeline_id, jobs_info

    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error fetching pipeline jobs information: {e}")
        return None

def create_and_send_traces(pipeline_info, jobs_info):
    # Create a new span for the entire operation
    with trace.get_tracer(__name__).start_as_current_span("gitlab_pipeline_operation"):
        # Create a span for the pipeline
        with trace.get_tracer(__name__).start_as_current_span("pipeline_info"):
            # Log pipeline information
            print("Pipeline Info:", pipeline_info)

        # Iterate through jobs and create spans
        print(jobs_info)
        for job_info in jobs_info:
            print(job_info)
            # Create a span for each job
            with trace.get_tracer(__name__).start_as_current_span("job_info") as span:
                # Log job information
                print("Job Info:", job_info)

                # Set additional attributes for the job span
                span.set_attribute("job_status", job_info["status"])

                # Capture start and end times (optional)
                #span.set_start_time(job_info.started_at)
                #span.set_end_time(job_info.finished_at)

    # Traces will be automatically sent to the OpenTelemetry Collector

def parse_args():
    parser = argparse.ArgumentParser(description='Get information for GitLab pipeline jobs.')
    parser.add_argument('pipeline_id',
            type=str, help='GitLab pipeline ID in the form "<project name>#<pipelineid>"')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()

    # Extract project name and pipeline id from the command line argument
    project_name, pipeline_id = args.pipeline_id.split('#')

    # Use the GitLab API to get the project ID based on the project name
    gl = gitlab.Gitlab('https://gitlab.com', private_token=os.getenv('PRIVATE_TOKEN'))
    project = gl.projects.get(project_name)
    project_id = project.id

    print(project_id, pipeline_id)

    otel_collector_endpoint = 'http://localhost:4317'

    pipeline_info, jobs_info = fetch_pipeline_jobs_info(gl, project_id, int(pipeline_id))

    if pipeline_info:
        # Initialize the OpenTelemetry tracer
        initialize_tracer(otel_collector_endpoint)

        # Create and send traces for the pipeline and jobs
        create_and_send_traces(pipeline_info, jobs_info)
